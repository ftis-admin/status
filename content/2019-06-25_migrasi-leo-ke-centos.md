---
title: Migrasi Leo ke CentOS [RESCHED]
date: '2019-06-25T14:00:00.010Z'
# scheduled: '2019-06-25T14:00:00.010Z'
# duration: '4320'
severity: partial-outage
affectedsystems:
  - judge
  - webapps
resolved: true
---
Migrasi ini dilakukan untuk mendukung driver terbaru dan meningkatkan stabilitas sistem.


::: update Migration Details | 2019-06-25T12:41:00.000Z
Kami akan memulai migrasi pada 16:00. Seluruh Web Apps vital akan di pindah ke
server lain. Proxy untuk publik akan di update setelah migrasi ini selesai.

Seluruh Judge akan mengalami Total Outtage, dan untuk sementara tidak dapat diakses.

Mohon dicatat bahwa propagasi DNS publik akan memakan waktu hingga 12 jam.
:::

::: update Rechedule | 2019-06-25T18:55:00.000Z
Migrasi direschedule ulang karena satu dan lain hal.
:::
<!--- language code: en -->
