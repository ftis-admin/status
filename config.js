module.exports = {
  "title": "LabFTIS",
  "name": "labftis",
  "description": "Laman Status untuk Layanan dan Server Lab Komputasi FTIS UNPAR",
  "defaultLocale": "en",
  "baseUrl":"https://status.labftis.net/",
  "locales": [
    {
      "code": "en",
      "iso": "en-US",
      "name": "English",
      "file": "en.json"
    }
  ],
  "content": {
    "frontMatterFormat": "yaml",
    systems: [ 'networking', 'judge', 'webapps', 'dc', "gitlab" ]
  },
  notifications: {
    support: {
      en: "https://gitlab.com/ftis-admin/labftis/issues"
    }
  },
  theme: {
    scheduled: { position: 'aboveSystems' },
    links: {
      en: {
        contact: "https://gitlab.com/ftis-admin/labftis/issues/new",
        gitlab: "https://gitlab.com/ftis-admin",
        github: "https://github.com/ifunpar",
        home: "https://informatika.unpar.ac.id"
      }
    }
  }
}